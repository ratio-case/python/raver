#########
Changelog
#########

********************
[3.0.2] - 2022-11-21
********************

Changed
=======

- Updated dependency constraints.


********************
[3.0.1] - 2022-07-12
********************

Fixed
=====

- Moved the git ref fetch inside of the text getting method, which helps make sure the
  ref actually exists (if it does on the optional remote).
- Only check for changes of the current repo against ref, ignoring whatever the current
  ref is (which may be some form of a (detached) HEAD).


********************
[3.0.0] - 2022-07-08
********************

Removed
=======

- Unused commands, essentially turning Raver back into the command it started with: version checking.


Changed
=======

- Single command ``raver`` now check for versions, potentially configured from the TOML
  or CLI interface.


******************
2.2.2 - 2022-06-08
******************

Changed
=======

- Loosened version string checker for changelogs.


*****
2.2.1
*****

* Re-added click to package dependencies.


*****
2.2.0
*****

* Changed the project layout to multiple modules. One per command and some utils.
* Raver is now packaged using the more mature poetry and supports checking the package
  version in there, too.


*****
2.1.1
*****

* Fixup current directory when calling doctests. It's now properly set to the docs path.


*****
2.1.0
*****

* ``raver test`` now also runs doctests.


*****
2.0.0
*****

* Adopt new Ratio package template setup.

  - Renamed documentation folder from ``doc`` to ``docs``, which reduces confusion with
    the ``raver docs`` command.
  - Moved to the Pydata sphinx theme.
  - Added some default Sphinx plugins and moved to the ``autoapi`` extension which does
    not need to import any package to generate docs.

* Fixed Python type hints to be more compliant to mypy.
* Moved to Python 3.8 by default.


*****
1.1.2
*****

* Fixup space before additional arguments in ``raver exe``.

*****
1.1.1
*****

* Add ``-o junit_family=xunit2`` to the default test options. This silences a warning
  by recent pytest 5.x versions that announce this new default value from 6.0.0.
* Moved from deprecated ``semver.parse_version_info()`` to
  ``semver.VersionInfo.parse()``.


*****
1.1.0
*****

* Allow tuning of the test coverage report using ``--cov`` and ``--cov-report`` from
  `pytest-cov <https://pytest-cov.readthedocs.io>`_.


*****
1.0.4
*****

* Make ``--script`` an option for ``raver install`` instead of an argument. This allows
  for a more flexible setup when relying on ``pyproject.toml`` files mostly.


*****
1.0.3
*****

* Fixup in install command. Should now work correctly in pipelines/linux.


*****
1.0.2
*****

* Improved CLI reporting to what's happening and whether it was successful for all
  commands.


*****
1.0.1
*****

* Fixup to docs (path to Windows' make.bat for sphinx).
* Added success message to Nuitka exe build including path to the executable.


*****
1.0.0
*****

* Completely new API with all the regular pipeline scripts included in ``raver`` itself.

  * Including TOML config! Take a look at the project's ``pyproject.toml`` for
    reference.

* The old functionality is now available under ``raver version``.



*****
0.2.3
*****

* Turns out ``git branch --show-current`` requires rather recent git versions to work,
  which isn't ideal to use in pipelines running all sorts of distros that often ship
  with older versions.


*****
0.2.2
*****

* Patched a bug where ``git symbolic-ref --short HEAD`` would fail.
  ``git branch --show-current`` does succeed and fits the job just fine.


*****
0.2.1
*****

* Added changelog checking. This checks whether a changelog entry exists for the current
  version. If not, a warning message is displayed informing you what to do and the
  process will exit with code 1 (error). Call with any of:

  ``raver --changelog /path/to/changelog.rst``

  ``raver -c /path/to/changelog.rst``

  ``raver --changelog /path/to/changelog/``

  ``raver -c /path/to/changelog/``

  Beware that it checks for the local version, such that the changelog check may pass
  even though the version still needs bumping (reference check fails).

  * If given a directory, it will scan for files in the format ``v0.0.0*``, where any
    suffix goes (star is a glob wildcard).

  * If given a path, it will scan the lines of text contained for **exactly and only**
    ``0.0.0``, e.g. without the ``v`` and no prefixes and suffixes allowed.
