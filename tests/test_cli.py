"""Test calls to raver with expected output."""

from pathlib import Path

from raver.cli import cli

root = Path(__file__).parent.parent


def test_version(runner, cwd):
    with cwd(root):
        res = runner.invoke(cli, "--version")
        assert res.exit_code == 0, res.output


def test_integral(runner, cwd, here):
    with cwd(here / "raw"):
        res = runner.invoke(cli, "-l changelog.rst")
        assert res.exit_code == 0, res.output

        res = runner.invoke(cli, "-l nonexistent.rst")
        assert res.exit_code == 2, res.output

        res = runner.invoke(cli, "-c nonexistent.toml")
        assert res.exit_code == 1, res.output

        res = runner.invoke(cli, "-r nonexistent/master")
        assert res.exit_code == 1, res.output

    with cwd(here):
        res = runner.invoke(cli, "-m nonexistent.py")
        assert res.exit_code == 2, res.output
