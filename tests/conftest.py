import os
from contextlib import contextmanager
from pathlib import Path

import click.testing
import pytest


@pytest.fixture
def runner():
    return click.testing.CliRunner(mix_stderr=True)


@pytest.fixture
def here():
    return Path(__file__).parent


@pytest.fixture
def cwd():
    @contextmanager
    def manager(path):
        oldpwd = os.getcwd()
        os.chdir(path)
        try:
            yield
        finally:
            os.chdir(oldpwd)

    return manager
